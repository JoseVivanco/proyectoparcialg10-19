/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import interfaz.*;
import java.util.*;

/** Declaracion de la clase Empresa
 *
 * @author Jose
 * @version 09/97/2019
 */
public class Empresa {
    //Declaracion de los atributos privados.
    private static Integer codigoIncremental = 1;
    private final Integer codigo;
    private String nombre;
    private String categoria;
    private String url;
    private String facebook;
    private String twitter;
    private String instagram;
    private final ArrayList<Establecimiento> establecimientos;
    
    /** Creacion del constructor.
     * 
     * @param nombre Indica el Nombre de la empresa.
     * @param categoria Indica la Categoria de la empresa.
     * @param url Indica el url del sitio web de la empresa.
     * @param facebook Indica el usurio de la pagina de Facebook de la empresa.
     * @param twitter Indica el usuario de Twitter de la empresa.
     * @param instagram Indica el usuario de Instragram de la empresa.
     */
    public Empresa(String nombre, String categoria, String url, String facebook, String twitter, String instagram) {
        this.codigo = Empresa.codigoIncremental++;
        this.nombre = nombre;
        this.categoria = categoria;
        this.url = url;
        this.facebook = facebook;
        this.twitter = twitter;
        this.instagram = instagram;
        this.establecimientos = new ArrayList<>();
    }
    /** Metodo comprobarEmpresa() determina su la empresa existe en el sistema.
     * 
     * @param nombreEmpresa Indica el nombre de la empresa a comprobar.
     * @return 
     */
    public static Empresa comprobarEmpresa(String nombreEmpresa) {
        Empresa empresa = null;
        for (Empresa e : Data.empresas) {
            if (e.getNombre().equalsIgnoreCase(nombreEmpresa)) {
                empresa = e;
            }
        }
        return empresa;
    }
    /** Metodo comprobarEmpresaNombre() busca y muestra empresas por sus nombres.
     * 
     * @param nombreEmpresa Indica el Nombre de la empresa a buscar.
     * @return 
     */
    public static ArrayList<Empresa> comprobarEmpresaNombre(String nombreEmpresa){
        ArrayList<Empresa> resultado = new ArrayList<>();
        for (Empresa e : Data.empresas) {
            if (e.getNombre().toLowerCase().contains(nombreEmpresa.toLowerCase())) {
                resultado.add(e);
            }
        }
        return resultado;
    }
    /** Metodo comprobarEmpresaCategoria() busca y muestra empresas por sus categorias.
     * 
     *
     * @param nCategoria Indica la Categoria de la empresa a buscar.
     * @return 
     */
    public static ArrayList<Empresa> comprobarEmpresaCategoria(String nCategoria) {
        ArrayList<Empresa> resultado = new ArrayList<>();
        for (Empresa e : Data.empresas) {
            if (e.getCategoria().toLowerCase().contains(nCategoria.toLowerCase())) {
                if (!(resultado.contains(e))){
                    resultado.add(e);
                }                
            }
        }
        return resultado;
    }
    /** Metodo comprobarEmpresaUbicacion() busca y muestra empresas por su ubicacion.
     * 
     * @param ubicacion Indica la Ubicacion de la empresa a buscar.
     * @return 
     */
    public static ArrayList<Establecimiento> comprobarEmpresaUbicacion(String ubicacion) {
        ArrayList<Establecimiento> retorno = new ArrayList<>();
        for (Empresa e : Data.empresas) {
            for (Establecimiento est : e.getEstablecimientos()) {
                if (est.getCiudad().toLowerCase().contains(ubicacion.toLowerCase())) {
                    if (!(retorno.contains(est))) {
                        retorno.add(est);
                    }
                }
                for (Sector s : Sector.values()) {
                    if (s.name().toLowerCase().contains(ubicacion.toLowerCase())) {
                        if (s.name().equalsIgnoreCase(est.getSector().name())) {
                            if (!(retorno.contains(est))) {
                                retorno.add(est);
                            }
                        }
                    }
                }
            }
        }
        return retorno;
    }
    /** Metodo registrarEmpresa() registra una empresa en el sistema.
     * 
     */
    public static void registrarEmpresa() {
        System.out.print("Ingrese el nombre de la empresa : ");
        String nuevoNombre = Util.toTitle(Util.ingresoString());
        Empresa empresa = comprobarEmpresa(nuevoNombre);
        if (empresa == null) {
            System.out.print("Ingrese categor�a : ");
            String nuevaCategoria = Util.capitalize(Util.ingresoString());
            System.out.print("Ingrese URL :");
            String nuevaUrl = Util.ingresoString().toLowerCase();
            System.out.print("Ingrese usuario de Facebook : ");
            String nuevoFacebook = Util.ingresoString();
            System.out.print("Ingrese usuario de Twitter : ");
            String nuevoTwitter = Util.ingresoString();
            System.out.print("Ingrese usuario de Instagram : ");
            String nuevoInstagram = Util.ingresoString();
            Empresa nuevaEmpresa = new Empresa(nuevoNombre, nuevaCategoria, nuevaUrl, nuevoFacebook, nuevoTwitter, nuevoInstagram);
            Data.empresas.add(nuevaEmpresa);
            System.out.println();
            System.out.println("Empresa agregada con �xito");
            Util.continuar();
        } else {
            System.out.println();
            System.out.println("La empresa ya existe");
            Util.continuar();
        }
    }

    /* GETTERS & SETTERS */
    public Integer getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public ArrayList<Establecimiento> getEstablecimientos() {
        return establecimientos;
    }

    @Override
    public String toString() {
        return "Empresa : " + nombre + "\n" + "Categor�a : " + categoria + "\n" + "Sitio web : " + url + "\n" + "Facebook : " 
                + facebook + "\n" + "Twitter : " + twitter + "\n" + "Instagram : " + instagram;
    }
}

